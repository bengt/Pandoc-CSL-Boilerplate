all: create clean

create:
	~/.cabal/bin/pandoc -s -S --bibliography Quellen.bib --csl ~/Projekte/github.com/citation-style-language/styles/harvard1de.csl Bachelorthesis.md --write latex -o Bachelorthesis.tex
	latex Bachelorthesis.tex
	pdflatex Bachelorthesis.tex

clean:
	rm *.aux *.log *.out *.tex *.dvi
	#rm *.bbl
