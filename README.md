# Pandoc CSL Boilerplate

Demonstration of Pandoc and CSL.

## Dependencies

-   Pandoc needs to be installed
-   CSL styles are needed

## Installing CSL/styles

    git clone git://github.com/citation-style-language/styles.git ~/Projekte/citation-style-language/styles

## Cloning this Repository

    git clone git@github.com:bigben87/Pandoc-CSL-Boilerplate.git

## Creating the PDF

    cd Pandoc-CSL-Boilerplate
    make

## Testing

The PDF should look like the one that can be found under downloads.

